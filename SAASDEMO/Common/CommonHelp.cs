﻿using Aliyun.Acs.Alidns.Model.V20150109;
using Aliyun.Acs.Core;
using Aliyun.Acs.Core.Exceptions;
using Aliyun.Acs.Core.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAASDEMO.Common
{
    public class CommonHelp
    {
        #region 云解析DNS-添加解析记录  可以去阿里云地址看文档 https://help.aliyun.com/document_detail/29772.html  但是这种方式服务器要求 80端口只允许部署这一套系统，因为现在这种方式是域名直接指向服务器IP
        public bool DomainNameResolution(string Name)
        {
            IClientProfile profile = DefaultProfile.GetProfile("cn-hangzhou", "", "");//域名 AccessKeyID Secret
            DefaultAcsClient client = new DefaultAcsClient(profile);

            var request = new AddDomainRecordRequest();
            request._Value = "";  //指向服务器IP
            request.Type = "A";
            request.RR = Name;  //随便定义   
            request.DomainName = "";  //域名
            try
            {
                var response = client.GetAcsResponse(request);
                return true;
                //Console.WriteLine(System.Text.Encoding.Default.GetString(response.HttpResponse.Content));
            }
            catch (ServerException e)
            {
                return false;
            }
            catch (ClientException e)
            {
                return false;
            }
        }
        #endregion
    }
}
