﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using SAASDEMO.Model;


namespace SAASDEMO.ViewModel.Ten.TenantVMs
{
    public partial class TenantBatchVM : BaseBatchVM<Tenant, Tenant_BatchEdit>
    {
        public TenantBatchVM()
        {
            ListVM = new TenantListVM();
            LinkedVM = new Tenant_BatchEdit();
        }

    }

	/// <summary>
    /// Class to define batch edit fields
    /// </summary>
    public class Tenant_BatchEdit : BaseVM
    {

        protected override void InitVM()
        {
        }

    }

}
