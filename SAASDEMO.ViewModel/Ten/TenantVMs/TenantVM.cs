﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using SAASDEMO.Model;


namespace SAASDEMO.ViewModel.Ten.TenantVMs
{
    public partial class TenantVM : BaseCRUDVM<Tenant>
    {
        public List<ComboSelectListItem> AllRoles { get; set; }

        public TenantVM()
        {
            SetInclude(x => x.Role);
        }

        protected override void InitVM()
        {
            AllRoles = DC.Set<FrameworkRole>().GetSelectListItems(Wtm, y => y.RoleName);
        }

        public override void DoAdd()
        {           
            base.DoAdd();
        }

        public override void DoEdit(bool updateAllFields = false)
        {
            base.DoEdit(updateAllFields);
        }

        public override void DoDelete()
        {
            base.DoDelete();
        }
    }
}
