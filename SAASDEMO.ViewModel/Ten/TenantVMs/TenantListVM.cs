﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using SAASDEMO.Model;


namespace SAASDEMO.ViewModel.Ten.TenantVMs
{
    public partial class TenantListVM : BasePagedListVM<Tenant_View, TenantSearcher>
    {
        protected override List<GridAction> InitGridAction()
        {
            return new List<GridAction>
            {
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.Create, Localizer["Sys.Create"],"Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.Edit, Localizer["Sys.Edit"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.Delete, Localizer["Sys.Delete"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.Details, Localizer["Sys.Details"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.BatchEdit, Localizer["Sys.BatchEdit"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.BatchDelete, Localizer["Sys.BatchDelete"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.Import, Localizer["Sys.Import"], "Ten", dialogWidth: 800),
                this.MakeStandardAction("Tenant", GridActionStandardTypesEnum.ExportExcel, Localizer["Sys.Export"], "Ten"),
            };
        }


        protected override IEnumerable<IGridColumn<Tenant_View>> InitGridHeader()
        {
            return new List<GridColumn<Tenant_View>>{
                this.MakeGridHeader(x => x.Code),
                this.MakeGridHeader(x => x.DomainName),
                this.MakeGridHeader(x => x.RoleName_view),
                this.MakeGridHeader(x => x.Account),
                this.MakeGridHeader(x => x.Name),
                this.MakeGridHeaderAction(width: 200)
            };
        }

        public override IOrderedQueryable<Tenant_View> GetSearchQuery()
        {
            var query = DC.Set<Tenant>()
                .CheckContain(Searcher.Code, x=>x.Code)
                .CheckEqual(Searcher.RoleId, x=>x.RoleId)
                .Select(x => new Tenant_View
                {
				    ID = x.ID,
                    Code = x.Code,
                    DomainName = x.DomainName,
                    RoleName_view = x.Role.RoleName,
                    Account = x.Account,
                    Name=x.Name,
                })
                .OrderBy(x => x.Code);
            return query;
        }

    }

    public class Tenant_View : Tenant{
        [Display(Name = "_Admin.RoleName")]
        public String RoleName_view { get; set; }

    }
}
