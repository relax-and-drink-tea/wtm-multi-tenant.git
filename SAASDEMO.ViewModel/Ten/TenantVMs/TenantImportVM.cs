﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using SAASDEMO.Model;


namespace SAASDEMO.ViewModel.Ten.TenantVMs
{
    public partial class TenantTemplateVM : BaseTemplateVM
    {
        [Display(Name = "编号")]
        public ExcelPropety Code_Excel = ExcelPropety.CreateProperty<Tenant>(x => x.Code);
        [Display(Name = "域名")]
        public ExcelPropety DomainName_Excel = ExcelPropety.CreateProperty<Tenant>(x => x.DomainName);
        [Display(Name = "租户角色")]
        public ExcelPropety Role_Excel = ExcelPropety.CreateProperty<Tenant>(x => x.RoleId);
        [Display(Name = "账号")]
        public ExcelPropety Account_Excel = ExcelPropety.CreateProperty<Tenant>(x => x.Account);

	    protected override void InitVM()
        {
            Role_Excel.DataType = ColumnDataType.ComboBox;
            Role_Excel.ListItems = DC.Set<FrameworkRole>().GetSelectListItems(Wtm, y => y.RoleName);
        }

    }

    public class TenantImportVM : BaseImportVM<TenantTemplateVM, Tenant>
    {

    }

}
