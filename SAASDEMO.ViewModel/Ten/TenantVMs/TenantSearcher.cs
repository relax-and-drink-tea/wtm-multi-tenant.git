﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;
using WalkingTec.Mvvm.Core.Extensions;
using SAASDEMO.Model;


namespace SAASDEMO.ViewModel.Ten.TenantVMs
{
    public partial class TenantSearcher : BaseSearcher
    {
        [Display(Name = "编号")]
        public String Code { get; set; }
        public List<ComboSelectListItem> AllRoles { get; set; }
        [Display(Name = "租户角色")]
        public Guid? RoleId { get; set; }

        protected override void InitVM()
        {
            AllRoles = DC.Set<FrameworkRole>().GetSelectListItems(Wtm, y => y.RoleName);
        }

    }
}
