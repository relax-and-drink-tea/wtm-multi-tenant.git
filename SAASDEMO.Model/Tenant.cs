﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WalkingTec.Mvvm.Core;

namespace SAASDEMO.Model
{
    public class Tenant : BasePoco
    {
        [Display(Name = "编号")]
        [Required(ErrorMessage = "{0}是必填项")]
        public string Code { get; set; }

        [Display(Name = "域名")]
        [Required(ErrorMessage = "{0}是必填项")]
        public string DomainName { get; set; }

        [Display(Name = "租户角色")]
        [Required(ErrorMessage = "{0}是必填项")]
        public Guid RoleId { get; set; }
        [Display(Name = "租户角色")]
        public FrameworkRole Role { get; set; }

        [Display(Name = "账号")]
        [Required(ErrorMessage = "{0}是必填项")]
        public string Account { get; set; }

        [Display(Name = "名称")]
        [Required(ErrorMessage = "{0}是必填项")]
        public string Name { get; set; }
    }
}
