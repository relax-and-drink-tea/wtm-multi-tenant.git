﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WalkingTec.Mvvm.Core;
using SAASDEMO.Controllers;
using SAASDEMO.ViewModel.Ten.TenantVMs;
using SAASDEMO.Model;
using SAASDEMO.DataAccess;


namespace SAASDEMO.Test
{
    [TestClass]
    public class TenantControllerTest
    {
        private TenantController _controller;
        private string _seed;

        public TenantControllerTest()
        {
            _seed = Guid.NewGuid().ToString();
            _controller = MockController.CreateController<TenantController>(new DataContext(_seed, DBTypeEnum.Memory), "user");
        }

        [TestMethod]
        public void SearchTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            string rv2 = _controller.Search((rv.Model as TenantListVM).Searcher);
            Assert.IsTrue(rv2.Contains("\"Code\":200"));
        }

        [TestMethod]
        public void CreateTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Create();
            Assert.IsInstanceOfType(rv.Model, typeof(TenantVM));

            TenantVM vm = rv.Model as TenantVM;
            Tenant v = new Tenant();
			
            v.Code = "joPNcKTC8P";
            v.DomainName = "hzASu2d45rT";
            v.RoleId = AddFrameworkRole();
            v.Account = "f";
            vm.Entity = v;
            _controller.Create(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Tenant>().Find(v.ID);
				
                Assert.AreEqual(data.Code, "joPNcKTC8P");
                Assert.AreEqual(data.DomainName, "hzASu2d45rT");
                Assert.AreEqual(data.Account, "f");
                Assert.AreEqual(data.CreateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.CreateTime.Value).Seconds < 10);
            }

        }

        [TestMethod]
        public void EditTest()
        {
            Tenant v = new Tenant();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
       			
                v.Code = "joPNcKTC8P";
                v.DomainName = "hzASu2d45rT";
                v.RoleId = AddFrameworkRole();
                v.Account = "f";
                context.Set<Tenant>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Edit(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(TenantVM));

            TenantVM vm = rv.Model as TenantVM;
            vm.Wtm.DC = new DataContext(_seed, DBTypeEnum.Memory);
            v = new Tenant();
            v.ID = vm.Entity.ID;
       		
            v.Code = "I3ER2Eph";
            v.DomainName = "XI4z";
            v.Account = "B5FEU";
            vm.Entity = v;
            vm.FC = new Dictionary<string, object>();
			
            vm.FC.Add("Entity.Code", "");
            vm.FC.Add("Entity.DomainName", "");
            vm.FC.Add("Entity.RoleId", "");
            vm.FC.Add("Entity.Account", "");
            _controller.Edit(vm);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Tenant>().Find(v.ID);
 				
                Assert.AreEqual(data.Code, "I3ER2Eph");
                Assert.AreEqual(data.DomainName, "XI4z");
                Assert.AreEqual(data.Account, "B5FEU");
                Assert.AreEqual(data.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data.UpdateTime.Value).Seconds < 10);
            }

        }


        [TestMethod]
        public void DeleteTest()
        {
            Tenant v = new Tenant();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
        		
                v.Code = "joPNcKTC8P";
                v.DomainName = "hzASu2d45rT";
                v.RoleId = AddFrameworkRole();
                v.Account = "f";
                context.Set<Tenant>().Add(v);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.Delete(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(TenantVM));

            TenantVM vm = rv.Model as TenantVM;
            v = new Tenant();
            v.ID = vm.Entity.ID;
            vm.Entity = v;
            _controller.Delete(v.ID.ToString(),null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data = context.Set<Tenant>().Find(v.ID);
                Assert.AreEqual(data, null);
          }

        }


        [TestMethod]
        public void DetailsTest()
        {
            Tenant v = new Tenant();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v.Code = "joPNcKTC8P";
                v.DomainName = "hzASu2d45rT";
                v.RoleId = AddFrameworkRole();
                v.Account = "f";
                context.Set<Tenant>().Add(v);
                context.SaveChanges();
            }
            PartialViewResult rv = (PartialViewResult)_controller.Details(v.ID.ToString());
            Assert.IsInstanceOfType(rv.Model, typeof(IBaseCRUDVM<TopBasePoco>));
            Assert.AreEqual(v.ID, (rv.Model as IBaseCRUDVM<TopBasePoco>).Entity.GetID());
        }

        [TestMethod]
        public void BatchEditTest()
        {
            Tenant v1 = new Tenant();
            Tenant v2 = new Tenant();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.Code = "joPNcKTC8P";
                v1.DomainName = "hzASu2d45rT";
                v1.RoleId = AddFrameworkRole();
                v1.Account = "f";
                v2.Code = "I3ER2Eph";
                v2.DomainName = "XI4z";
                v2.RoleId = v1.RoleId; 
                v2.Account = "B5FEU";
                context.Set<Tenant>().Add(v1);
                context.Set<Tenant>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(TenantBatchVM));

            TenantBatchVM vm = rv.Model as TenantBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            
            vm.FC = new Dictionary<string, object>();
			
            _controller.DoBatchEdit(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data1 = context.Set<Tenant>().Find(v1.ID);
                var data2 = context.Set<Tenant>().Find(v2.ID);
 				
                Assert.AreEqual(data1.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data1.UpdateTime.Value).Seconds < 10);
                Assert.AreEqual(data2.UpdateBy, "user");
                Assert.IsTrue(DateTime.Now.Subtract(data2.UpdateTime.Value).Seconds < 10);
            }
        }


        [TestMethod]
        public void BatchDeleteTest()
        {
            Tenant v1 = new Tenant();
            Tenant v2 = new Tenant();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
				
                v1.Code = "joPNcKTC8P";
                v1.DomainName = "hzASu2d45rT";
                v1.RoleId = AddFrameworkRole();
                v1.Account = "f";
                v2.Code = "I3ER2Eph";
                v2.DomainName = "XI4z";
                v2.RoleId = v1.RoleId; 
                v2.Account = "B5FEU";
                context.Set<Tenant>().Add(v1);
                context.Set<Tenant>().Add(v2);
                context.SaveChanges();
            }

            PartialViewResult rv = (PartialViewResult)_controller.BatchDelete(new string[] { v1.ID.ToString(), v2.ID.ToString() });
            Assert.IsInstanceOfType(rv.Model, typeof(TenantBatchVM));

            TenantBatchVM vm = rv.Model as TenantBatchVM;
            vm.Ids = new string[] { v1.ID.ToString(), v2.ID.ToString() };
            _controller.DoBatchDelete(vm, null);

            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                var data1 = context.Set<Tenant>().Find(v1.ID);
                var data2 = context.Set<Tenant>().Find(v2.ID);
                Assert.AreEqual(data1, null);
            Assert.AreEqual(data2, null);
            }
        }

        [TestMethod]
        public void ExportTest()
        {
            PartialViewResult rv = (PartialViewResult)_controller.Index();
            Assert.IsInstanceOfType(rv.Model, typeof(IBasePagedListVM<TopBasePoco, BaseSearcher>));
            IActionResult rv2 = _controller.ExportExcel(rv.Model as TenantListVM);
            Assert.IsTrue((rv2 as FileContentResult).FileContents.Length > 0);
        }

        private Guid AddFrameworkRole()
        {
            FrameworkRole v = new FrameworkRole();
            using (var context = new DataContext(_seed, DBTypeEnum.Memory))
            {
                try{

                v.RoleCode = "qkGKcfvLZyIbPUwaSc3iQJpslbnKqbqyM9jpusA800MUr7jpxgtHJp9g5YiB3kf8ITOQ";
                v.RoleName = "u8vHgBCaTEi0L9X4ZBLd";
                v.RoleRemark = "z4O2hfUZxj";
                v.TenantCode = "yngefhGEwy3unqN9dzm";
                context.Set<FrameworkRole>().Add(v);
                context.SaveChanges();
                }
                catch{}
            }
            return v.ID;
        }


    }
}
